
use super::SessionID;
use super::InstanceID;
use super::ModuleID;

pub struct Session {
	pub id: SessionID,
	pub source: Option<InstanceID>,
	pub target: InstanceID,
	pub module: ModuleID,
}

impl From<(Option<InstanceID>, InstanceID, ModuleID)> for Session {
	fn from(args: (Option<InstanceID>, InstanceID, ModuleID)) -> Session {
		Session {
			id: gen_sid(),
			source: args.0,
			target: args.1,
			module: args.2,
		}
	}
}

fn gen_sid() -> SessionID {
	use rand;
	let mut buf = [0; 32];
	for i in 0..32 {
		let unit = rand::random::<u8>();
		buf[i] = unit;
	}
	buf
}
