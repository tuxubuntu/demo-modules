
use core::Core;
use SessionID;
use drivers::pipe_driver::SimpleDriver;

#[test]
fn it_works() {
	use std::thread;
	use std::time;
	let mut core = Core::new();
	let driver = SimpleDriver::new();
	core.reg_driver(driver).unwrap();
	let mut lock = core.init().unwrap();
	let receiver = lock.receiver().unwrap();
	let mlist = core.modules();
	let mid = mlist[0];
	let sid: Option<SessionID> = None;
	let sid = core.open_session(sid, mid, true).unwrap();
	thread::spawn(move || {
		thread::sleep(time::Duration::from_millis(1000));
		use std::process;
		process::exit(0);
	});
	core.send(sid.clone(), b"lalka".to_vec()).unwrap();
	let mut i = 0;
	for msg in receiver.iter() {
		println!("\t>>{:?}", msg);
		thread::sleep(time::Duration::from_millis(10));
		core.send(sid.clone(), msg.1).unwrap();
		if i > 10 {
			break;
		} else {
			i += 1;
		}
	}
	lock.wait().unwrap();
	core.close_session(sid).unwrap();
}
