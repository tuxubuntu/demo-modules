extern crate containerizer;
extern crate message;

extern crate rand;

pub mod error;
pub mod core;
pub mod drivers;

use error::Error;

type Data = Vec<u8>;
type Result<T> = std::result::Result<T, Error>;

pub type DriverID = [u8; 16];
pub type ModuleID = [u8; 16];
pub type SessionID = [u8; 32];

pub trait Closer {
	fn down(&mut self) -> Result<()>;
}

pub trait Reader where Self: Send {
	fn read(&mut self, usize) -> Result<Data>;
}

pub trait Writer where Self: Send {
	fn write(&mut self, &Data) -> Result<()>;
}

pub trait Instance where Self: Closer+Send {
	fn split(&mut self) -> Result<(Box<Reader>, Box<Writer>)>;
}

pub trait Driver {
	fn id(&self) -> DriverID;
	fn init(&mut self) -> Result<()>;
	fn modules(&self) -> Vec<ModuleID>;
	fn up_instance(&mut self, ModuleID) -> Result<Box<Instance>>;
}

#[cfg(test)]
mod tests;
