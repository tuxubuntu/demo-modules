
mod instance;

use std::process::Child;
use std::collections::HashMap;

use self::instance::SimpleInstance;


use Instance;
use Driver;
use Result;
use DriverID;
use ModuleID;



pub struct SimpleDriver {
	modules: HashMap<ModuleID, (&'static str, Vec<&'static str>)>,
}

impl SimpleDriver {
	pub fn new() -> SimpleDriver {
		SimpleDriver {
			modules: HashMap::new(),
		}
	}
}

const DID: DriverID = [
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
];

const MID: ModuleID = [
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
];


impl Driver for SimpleDriver {
	fn id(&self) -> DriverID {
		DID
	}
	fn modules(&self) -> Vec<ModuleID> {
		self.modules.keys().map(|&x| x).collect()
	}
	fn init(&mut self) -> Result<()> {
		// self.modules.insert(MID, ("docker", vec![ "run", "--rm", "-i", "alpine", "cat" ]));
		self.modules.insert(MID, ("cat", vec![]));
		Ok(())
	}
	fn up_instance(&mut self, mid: ModuleID) -> Result<Box<Instance>> {
		let (name, args) = self.modules.get(&mid)
			.ok_or("Module not found.")?;
		let child = exec(name, args.clone())?;
		Ok(Box::new(SimpleInstance::from(child)))
	}
}


fn exec(name: &str, args: Vec<&str>) -> Result<Child> {
	use std::process::Stdio;
	use std::process::Command;
	let child = Command::new(name)
		.args(args)
		.stdin(Stdio::piped())
		.stdout(Stdio::piped())
		.spawn()?;
	Ok(child)
}
